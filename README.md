# file-exists [![NPM version](https://badge.fury.io/js/file-exists.svg)](https://npmjs.org/package/file-exists) [![Build Status](https://travis-ci.org/doowb/file-exists.svg?branch=master)](https://travis-ci.org/doowb/file-exists)

> Simple function to check if a file exists at a GitHub URL

## Installation

```sh
$ npm install --save file-exists
```

## Usage

```js
var fileExists = require('file-exists');
fileExists();
```

## License

MIT © [Brian Woodward](https://doowb.com)
