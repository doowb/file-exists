'use strict';

const axios = require('axios');

module.exports = (req, res) => {
  const validate = async ({ repo, path }) => {
    return typeof repo === 'string' && typeof path === 'string';
  };

  const check = async ({ repo, path }) => {
    const url = `https://github.com/${repo}/${path}`;
    const format = result => {
      let fileLink = `<a href="https://github.com/${repo}/${path}">${path}</a>`;
      let repoLink = `<a href="https://github.com/${repo}">${repo}</a>`;

      result.summary = `
        <p>The ${fileLink} file does${result.exists ? '' : ' not'} exist for ${repoLink}</p>
      `;
      return result;
    };

    return axios.get(url)
      .then(res => format({ exists: true }))
      .catch(err => {
        if (/404/.test(err.message)) {
          return format({ exists: false });
        }
        throw err;
      });
  };

  if (validate(req.query)) {
    return check(req.query);
  }

  res.status(400);
  res.send({ error: 'Invalid parameters.' });
};
